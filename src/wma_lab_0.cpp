#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    cout << "CV_MAJOR = " << CV_MAJOR_VERSION << endl;
    cout << "CV_MINOR = " << CV_MINOR_VERSION << endl;
    cout << "============= TEST PASSED =============" << endl;

    return 0;
}
