#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    // Wczytanie
    string path = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_2/1.jpg";
    Mat input = imread(path);

    // Zmniejszenie wymiarow do 0.5
    pyrDown(input, input);

    // Wyswietl input
    imshow("input", input);

    // Stworzenie obrazu do wizualizacji
    Mat output = input.clone();

    // Przejscie na skale szarosci
    Mat gray;
    cvtColor(input, gray, CV_BGR2GRAY);

    // Progowanie adaptacyjne
    adaptiveThreshold(gray, gray, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 101,-35);

    // Morfologiczne otwarcie
    morphologyEx(gray, gray, MORPH_OPEN, Mat());

    // Morfologiczne zamkniecie z kolowym elementem strukturalnym
    Mat kernel = getStructuringElement(CV_SHAPE_ELLIPSE, Size(7,7));
    morphologyEx(gray, gray, MORPH_CLOSE, kernel);

    // Wyznaczenie konturow
    vector<vector<Point> > contours;
    findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

    // Analiza powierzchni konturow
    int count = 0;
    for (int i = 0; i<contours.size(); i++)
    {
        float area = contourArea(contours[i]);
        if (area > 50)
        {
            drawContours(output, contours, i, Scalar(255,0,0), 2, 8);
            count++;
        }
    }

    // Pokazanie wynikow
    cout << "Wykryto " << count << " obiektow." << endl;
    imshow("Out", output);
    waitKey(0);

    // Return
    return 0;
}
