#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

/**
 * @brief loadAndDisplay        Testowanie kilku roznych sposobow wczytywania obrazu.
 */
int loadAndDisplay()
{
    // Zakladamy, ze plik wykonywalny jest w folderze build,
    // wtedy mozemy podac sciezke wzgledna do pliku
    Mat img_rel = imread("../data/wm_lab_1/gandalf.png");

    // Mozna podac bezwzgledna sciezke do pliku
    Mat img_abs = imread("/home/daniel/wm_labs/data/wm_lab_1/gandalf.png");

    // Mozna uzyc zmiennej cmake dodanej do makefile'a jako definicja
    // (patrz CMakeLists.txt) - rozwiazanie najbardziej eleganckie
    // i uniwersalne z powyzszych
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka wzgledna
    if (img_rel.empty())
    {
        cout << "ERROR: Can't open image with relative path" << endl;
        return -1;
    }

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka bezwzgledna
    if (img_abs.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Imshow
    imshow("gandalf_rel", img_rel);
    imshow("gandalf_abs", img_abs);
    imshow("gandalf_def", img_def);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

/**
 * @brief funWithContrast       Dokonaj zmiany kontrastu wczytanego obrazu
 */
int funWithContrast(int alfa, int beta)
{
    // Wczytanie obrazu testowego (patrz @loadAndDisplay)
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Dokonaj zmiany kontrastu
    Mat img_out = alfa*img_def + beta;

    // Imshow
    imshow("contrast_src", img_def);
    imshow("contrast_dst", img_out);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

/**
 * @brief funWithThreshold      Dokonaj operacji binaryzacji.
 */
int funWithThreshold(int val)
{
    // Wczytanie obrazu testowego (patrz @loadAndDisplay),
    // dodatkowo wczytujemy obraz jako czarno-biały
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def, IMREAD_GRAYSCALE);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Dokonaj binaryzacji
    Mat img_out;
    threshold(img_def, img_out, val, 255, THRESH_BINARY);

    // Imshow
    imshow("threshold_src", img_def);
    imshow("threshold_dst", img_out);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

/**
 * @brief funWithMorph          Przyklad wykorzystania operacji morfologicznych.
 */
int funWithMorph(int kernel_size)
{
    // Wczytanie obrazu testowego (patrz @loadAndDisplay)
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Dylatacja z wykorzystaniem domyslnego elementu strukturalnego
    Mat img_dil_default;
    dilate(img_def, img_dil_default, Mat());

    // Dylataja z wykorzystaniem wlasnego elementu strukturalnego
    Mat img_dil_kernel;
    Mat morph_kernel = getStructuringElement(MORPH_ELLIPSE, Size(kernel_size, kernel_size));
    dilate(img_def, img_dil_kernel, morph_kernel);

    // Erozja z wykorzystaniem domyslnego elementu strukturalnego
    Mat img_ero_default;
    erode(img_def, img_ero_default, Mat());

    // Dylataja z wykorzystaniem wlasnego elementu strukturalnego
    Mat img_ero_kernel;
    erode(img_def, img_ero_kernel, morph_kernel);

    // Imshow
    imshow("morph_src", img_def);
    imshow("morph_dil_default", img_dil_default);
    imshow("morph_dil_kernel", img_dil_kernel);
    imshow("morph_ero_default", img_ero_default);
    imshow("morph_ero_kernel", img_ero_kernel);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

/**
 * @brief funWithFiltering      Przyklad filtracji obrazu.
 */
int funWithFiltering(int kernel_size)
{
    // Wczytanie obrazu testowego (patrz @loadAndDisplay)
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Dylatacja z wykorzystaniem domyslnego elementu strukturalnego
    Mat img_median;
    medianBlur(img_def, img_median, kernel_size);

    // Dylataja z wykorzystaniem wlasnego elementu strukturalnego
    Mat img_gaussian;
    float sigma = 0.3*((kernel_size-1)*0.5 - 1) + 0.8; // zobacz @getGaussianKernel w dokumentacji opencv
    GaussianBlur(img_def, img_gaussian, Size(kernel_size, kernel_size), sigma);

    // Imshow
    imshow("filtering_src", img_def);
    imshow("filtering_median", img_median);
    imshow("filtering_gaussian", img_gaussian);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

/**
 * @brief funWithMasks          Definicja maski oraz proste dzialania z jej wykorzystaniem.
 */
int funWithMasks()
{
    // Wczytanie obrazu testowego (patrz @loadAndDisplay)
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Tworzenie maski
    Mat mask(img_def.rows, img_def.cols, CV_8UC1);

    // Ustawiamy, by maska dzialala wszedzie oprocz okregu
    mask.setTo(255);
    circle(mask, Point(610, 390), 100, Scalar(0), -1);

    // Skopiuj zdjecie i ustaw 0 wszedzie tam, gdzie maska jest niezerowa
    Mat img_mask = img_def.clone();
    img_mask.setTo(0, mask);

    // Imshow
    imshow("mask_src", img_def);
    imshow("mask_mask", mask);
    imshow("mask_final", img_mask);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

/**
 * @brief funWithPixelIteration Iteracja po pixelach (rysowanie przekatnych).
 */
int funWithPixelIteration()
{
    // Wczytanie obrazu testowego (patrz @loadAndDisplay)
    string path_def = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_1/gandalf.png";
    Mat img_def = imread(path_def);

    // Sprawdzenie, czy udalo sie otworzyc obraz z sciezka z uzyciem define
    if (img_def.empty())
    {
        cout << "ERROR: Can't open image with absolute path" << endl;
        return -1;
    }

    // Skopiuj zdjecie
    Mat img_diag = img_def.clone();

    //Petla podwojna
    float diag_ratio = ((float)img_diag.rows)/img_diag.cols;    // tangens kata nachylenia przekatnej
    float diag_tolerance = 2.0;                                 // grubosc przekatnej
    for (int h=0; h<img_diag.rows; ++h)
        for (int w=0; w<img_diag.cols; ++w)
        {
            float y1 = w * diag_ratio;                          // jaka wysokosc mialby punkt na rownoleglej do przekatnej
            float y2 = (img_diag.cols - w) * diag_ratio;        // jaka wysokosc mialby punkt na rownoleglej do drugiej przekatnej
            if (abs(y1 - h) < diag_tolerance || abs(y2 - h) < diag_tolerance)       // czy to jest przekatna?
            {
                // Sposob pierwszy (mozna wybrac jeden)
                img_diag.at<Vec3b>(h,w) = Vec3b(25, 125, 225);

                // Sposob drugi
                img_diag.at<Vec3b>(h,w)[0] = 25;
                img_diag.at<Vec3b>(h,w)[1] = 125;
                img_diag.at<Vec3b>(h,w)[2] = 225;

                // Sposob trzeci
                Vec3b& pixel_ref = img_diag.at<Vec3b>(h,w);
                pixel_ref[0] = 25;
                pixel_ref[1] = 125;
                pixel_ref[2] = 255;

                // Sposob czwarty
                Vec3b* pixel_ptr = &img_diag.at<Vec3b>(h,w);
                (*pixel_ptr)[0] = 25;
                (*pixel_ptr)[1] = 125;
                (*pixel_ptr)[2] = 255;
            }
        }

    // Imshow
    imshow("pixiter_src", img_def);
    imshow("pixiter_dst", img_diag);
    waitKey(0);

    // Ret
    destroyAllWindows();
    return 0;
}

int main(int argc, char** argv)
{
    loadAndDisplay();
    funWithContrast(2, -40);
    funWithThreshold(55);
    funWithMorph(7);
    funWithFiltering(7);
    funWithMasks();
    funWithPixelIteration();
    return 0;
}
