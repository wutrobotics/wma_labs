#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace std;
using namespace cv;

// Punkt wcisniecia L przycisku
Point down_pt;

// Zaznaczenie tymczasowe oraz stabilne
Rect selection_unstable, selection_stable;

// 0 - brak, 1 - zaznaczanie, 2 - zaznaczono
int selecting_status = 0;

// Funkcja obslugi zdarzen myszki
void mouseCb(int event, int x, int y, int flags, void* userdata)
{
    // Current
    Point curr_pt = Point(x, y);

    // Jezeli wcisnieto lewy przycisk myszy
    if (event == EVENT_LBUTTONDOWN)
    {
        cout << "LMB DOWN" << endl;
        selecting_status = 1;
        down_pt = curr_pt;

        // Jezeli wycisnieto lewy przycisk myszy
    } else if (event == EVENT_LBUTTONUP)
    {
        cout << "LMB UP" << endl;
        selecting_status = 2;
        selection_stable = selection_unstable;
    }

    // Oblicz zaznaczenie
    Point corner_a = Point( min(down_pt.x, curr_pt.x), min(down_pt.y, curr_pt.y) );
    Point corner_b = Point( max(down_pt.x, curr_pt.x), max(down_pt.y, curr_pt.y) );
    selection_unstable = Rect( corner_a.x, corner_a.y, corner_b.x-corner_a.x, corner_b.y-corner_a.y );
}


int main(int argc, char** argv)
{
    // Stworzenie okna interfejsu
    namedWindow("Interfejs", 1);

    // Zarejestrowanie funkcji obslugi zdarzen myszki
    setMouseCallback("Interfejs", mouseCb);

    // Otwarcie filmu
    string path = string(PROJECT_SOURCE_DIR) + "/data/wm_lab_3/1.webm";
    VideoCapture capture(path);

    // Obecna klatka, poprzednia, przetwarzana
    Mat frame, prev, img;

    // ================================================================
    // ========================== INTERFEJS ===========================
    // ================================================================

    // Pobranie pierwszej klatki
    if(!capture.read(frame)) return -1;
    Mat selection_mask = Mat(frame.rows, frame.cols, CV_8UC1);
    Mat output;

    // Petla do wyswietlania interfejsu
    while(true)
    {
        // Narysowanie zaznaczenia
        output = frame.clone();
        selection_mask.setTo(0);
        if(selecting_status == 1)
        {
            rectangle(selection_mask, selection_unstable, Scalar(255,0,0));
            output.setTo(Scalar(255,0,0), selection_mask);
        } else if (selecting_status == 2)
        {
            rectangle(selection_mask, selection_stable, Scalar(255,0,0), -1);
            add(output, Scalar(255,0,0), output, selection_mask);
        }

        // Wyswietlenie
        imshow("Interfejs", output);

        // Czekanie na klawisz 33 ms
        char c = waitKey(33);
        // Zapisanie poprzedniej klatki
            cvtColor( frame, prev, CV_BGR2GRAY );
        // Jezeli wcisnieto escape - koniec
        if(c == 27) break;
    }

    // Zapisanie poprzedniej klatki
    cvtColor(frame, prev, CV_BGR2GRAY);

    // ================================================================
    // ====================== PRZETWORZENIE FILMU =====================
    // ================================================================

    // Poprzednia pozycja
    Point prev_pos(9999, 9999);

    // Punkt trafienia
    Point hit_point(-1,-1);

    // Wektor punktow do wizualizacji
    vector<Point> vis_points;

    while(true)
    {
        // Sprawdzenie czy jest koniec filmu
        if(!capture.read(frame)) break;

        // Wizualizacja tarczy
        output = frame.clone();
        rectangle(selection_mask, selection_stable, Scalar(255,0,0), -1);
        add(output, Scalar(255,0,0), output, selection_mask);

        // Skala szarosci
        cvtColor(frame, frame, CV_BGR2GRAY);

        // Roznicowanie klatek
        absdiff(frame, prev, img);
        // Binaryzacja
        threshold(img, img, 20, 255, THRESH_BINARY);
        // Morfologiczne otwarcie
        morphologyEx(img, img, MORPH_OPEN, Mat());
        // Wyznaczenie konturow
        vector<vector<Point> > contours;
        findContours(img, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

        // Analiza konturow
        Point pos(-1,-1);
        for (int i = 0; i<contours.size(); i++)
        {
            float S = contourArea(contours[i]);
            float L = arcLength(contours[i], true);
            // Wspolczynnik Malinowskiej
            float M = L/(2*sqrt(M_PI*S))-1;
            if (S > 50 && M < 0.5)
            {
                Moments mu = moments(contours[i], false);
                pos = Point(mu.m10/mu.m00 , mu.m01/mu.m00);
                //drawContours(output, contours, i, Scalar(0,0,255), 2, 8);
                break;
            }
        }

        // Jesli znaleziono kontur
        if (pos.x > 0)
        {
            // Sprawdzenie, czy obiekt szybko spada
            if (pos.y > prev_pos.y + 20)
            {
                hit_point = prev_pos;
                break;
            }
            // Zapisanie poprzedniej pozycji obiektu
            prev_pos = pos;
            // Dodanie punktu do wizualizacji
            vis_points.push_back(pos);
        }

        // Narysowanie trajektorii
        for (int i=0; i<vis_points.size(); i++)
            circle(output, vis_points[i], 2, Scalar(0,100,255), -1);

        // Zapisanie poprzedniej klatki
        prev = frame.clone();

        // Wyswietlenie
        imshow("Interfejs", output);
        // Czekanie na klawisz 33 ms
        char c = waitKey(33);
        // Jezeli wcisnieto spacje - koniec
        if (c == 27) break;
    }

    // Jesli nie wykryto spadania, uwzgledniamy ostatnia wykryta pozycje
    if (hit_point.x < 0)
        hit_point = prev_pos;

    // Sprawdzenie, czy trafiono
    bool hit = false;
    if (hit_point.x > selection_stable.x && hit_point.y > selection_stable.y &&
        hit_point.x < selection_stable.x+selection_stable.width &&
        hit_point.y < selection_stable.y+selection_stable.height)
            hit = true;

    // Narysowanie wyniku - zielone lub czerwone kolo
    if (hit) circle(output, hit_point, 20, Scalar(0,255,0), 2);
    else circle(output, hit_point, 20, Scalar(0,0,255), 2);
    imshow("Interfejs", output);

    // Return
    waitKey(0);
    return 0;
}


//============ PRZYDATNE FUNKCJE ============

// Pelne skopiowanie obrazu (np. do wizualizacji)
//Mat output = frame.clone();

// Wartosc bezwzgledna roznicy dwoch obrazow
//absdiff( wejscie1, wejscie2, wyjscie );

// Momenty geometryczne (np. do wyznaczenia srodka konturu)
//moments
