### What is this repository for? ###

The Machine Vision laboratories source code.

### How to set up in linux? ###

    $ sudo apt-get install libopencv-dev qtcreator cmake
    $ git clone https://bitbucket.org/wutrobotics/wm_labs.git
    $ cd wm_labs && mkdir build && cd build
    $ cmake ../
    $ make

### How to set up in windows? ###

Dunno - get yourself a linux or at least a Virtual Machine :)