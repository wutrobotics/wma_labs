cmake_minimum_required(VERSION 2.8.3)
project(wma_labs)

# Find OpenCV
find_package(OpenCV REQUIRED)
if (OpenCV_FOUND)
    message("-- OpenCV ${OpenCV_VERSION} was found")
    include_directories(${OpenCV_INCLUDE_DIRS})
else(OpenCV_FOUND)
    message(FATAL_ERROR "OpenCV was not found")
endif(OpenCV_FOUND)

# Definitions
add_definitions(-DPROJECT_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}\")

# Declare a cpp executable (lab3)
add_executable(wma_lab_3 src/wma_lab_3.cpp)
# Specify libraries to link a library or executable target against (lab3)
target_link_libraries(wma_lab_3 ${OpenCV_LIBS})

# Declare a cpp executable (lab2)
add_executable(wma_lab_2 src/wma_lab_2.cpp)
# Specify libraries to link a library or executable target against (lab2)
target_link_libraries(wma_lab_2 ${OpenCV_LIBS})

# Declare a cpp executable (lab1)
add_executable(wma_lab_1 src/wma_lab_1.cpp)
# Specify libraries to link a library or executable target against (lab1)
target_link_libraries(wma_lab_1 ${OpenCV_LIBS})

# Declare a cpp executable (lab0)
add_executable(wma_lab_0 src/wma_lab_0.cpp)
# Specify libraries to link a library or executable target against (lab0)
target_link_libraries(wma_lab_0 ${OpenCV_LIBS})
